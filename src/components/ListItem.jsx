import React from 'react';

export default class ListItem extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <li>
        <h4>{this.props.text}</h4>
      </li>
    )
  }
}
