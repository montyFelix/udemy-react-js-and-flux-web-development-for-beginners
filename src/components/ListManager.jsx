import React from 'react';
import List from './List.jsx'

export default class ListManager extends React.Component {
  constructor(props) {
    super(props);
    this.state = {'items': [], 'newItemText': ''};
    this.onChange = this.onChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.escapeHtml = this.escapeHtml.bind(this);
  }

  escapeHtml(text) {
  const map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };
  return text.replace(/[&<>"']/g, (m) => map[m]);
  }

  onChange(e) {
    this.setState({newItemText: e.target.value});
  }
  handleSubmit(e) {
    e.preventDefault();

    let currentItems = this.state.items;
    let newItem = this.escapeHtml(this.state.newItemText);
    currentItems.push(newItem);
    this.setState({'items': currentItems, 'newItemText':''});
  }
  render() {
    let divStyle = {
      marginTop: 10
    }

    let headingStyle = {

    }

    if(this.props.headingColor) {
      headingStyle.background = this.props.headingColor
    }

    return (
      <div style={divStyle} className="col-sm-12 col-md-4">
        <div className="panel panel-primary">
          <div style={headingStyle} className="panel-heading">
            <h3>{this.props.title}</h3>
          </div>
          <div className="row panel-body">
            <form onSubmit={this.handleSubmit}>
              <div className="col-xs-8 col-sm-10 col-md-9">
                <input className="form-control" onChange={this.onChange} value={this.state.newItemText} />
              </div>
              <div className="col-xs-4 col-sm-2 col-md-3">
                <button className="btn btn-primary">Add</button>
              </div>
            </form>
            </div>
            <List items={this.state.items} />

        </div>
      </div>
    );
  }

}
