import React from 'react';
import reactDOM from 'react-dom';

import Routes from './Routes.jsx';

reactDOM.render(Routes, document.getElementById('main'));
