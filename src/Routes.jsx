import React from 'react';
import {BrowserRouter as Router, Route, Link } from 'react-router-dom';

import Base from './components/Base.jsx';
import Page1 from './components/Page1.jsx';
import Page2 from './components/Page2.jsx';

const Routes = (
  <Router>
    <div>
      <ul>
        <li><Link to="/">Home</Link></li>
        <li><Link to="/page1">page1</Link></li>
        <li><Link to="/page2">Topics</Link></li>
      </ul>

      <Route exact path="/" component={Base} />
      <Route path="/page1" component={Page1} />
      <Route path="/page2" component={Page2} />
    </div>
  </Router>
)

export default Routes;
